<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('usr_id');
            $table->string('usr_name');  
            $table->string('usr_username')->unique();  
            $table->string('usr_email')->unique();
            $table->string('password');
            $table->string('usr_city');
            $table->integer('pro_id')->unsigned()->index();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('pro_id')->references('pro_id')->on('profiles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }
}
