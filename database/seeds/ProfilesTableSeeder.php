<?php

use Illuminate\Database\Seeder;
use App\Profile;

class ProfilesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Profile::create([
            'pro_name' => 'administrator'
        ]);

        Profile::create([
            'pro_name' => 'user'
        ]);
    }
}
