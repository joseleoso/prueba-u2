<?php

namespace App\Http\Controllers;

use App\User;
use App\Hobby;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('home');
    }


    public function profile() {
        $user = Auth::user();

        $arrHobbies = array();

        foreach ($user->hobbies as $hobby) {
            $arrHobbies[] = ucfirst(mb_strtolower($hobby->hob_name, 'UTF-8'));
            $user->hobby = implode(", ", $arrHobbies);
        }

        return view('profile', compact('user'));
    }

    public function profile_update(Request $request){
        $user = Auth::user();

        $user->update($request->all());

        $hobbies = $request->get('hobbies');
        $arrHobbies = explode(',', $hobbies);
        $arrHobbie2 = array();

        foreach ($arrHobbies as $hobby) {
            $qHobby = Hobby::where('hob_name', trim(mb_strtoupper($hobby, 'UTF-8')))->first();
            if(!$qHobby){
                $insHobby = Hobby::create(['hob_name' => trim(mb_strtoupper($hobby, 'UTF-8'))]);
                $arrHobbie2[] = $insHobby->hob_id;
            } else {
                $arrHobbie2[] = $qHobby->hob_id;
            }
        }

        $user->hobbies()->sync($arrHobbie2);

        return redirect('profile')->with('status', 'Perfil Actualizado!');
    }
}
