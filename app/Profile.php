<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model{
    protected $primaryKey = 'pro_id';

    protected $fillable = [
        'pro_name'
    ];

    public function users(){
        return $this->hasMany('App\User');
    }
}
