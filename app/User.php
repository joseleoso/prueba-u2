<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    protected $primaryKey = 'usr_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usr_username', 'usr_name', 'usr_email', 'password', 'usr_city', 'pro_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getAuthIdentifierName(){
        return 'usr_id';
    }

    public function profile(){
        return $this->belongsTo('App\Profile', 'pro_id', 'pro_id');
    }

    public function hobbies(){
        return $this->belongsToMany('App\Hobby', 'users_hobbies', 'usr_id', 'hob_id');
    }

}
